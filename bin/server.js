var url = require('url');
var http = require('http');
var path = require('path');
var fs = require('fs');
var port = process.env.HOST_PORT ? process.env.HOST_PORT : 8080;


http.createServer(function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');
    if ( req.method === 'OPTIONS' ) {
        res.writeHead(200);
        res.end();
        return;
    }
    var url_parts = url.parse(req.url);
    var filePath = "";
    req.setEncoding('utf8');

    if (url_parts.pathname == '/next' && req.method == "POST") {
        saveTickets(req, res);
        return;
    }
    else if (url_parts.pathname == '/data') {
        filePath = "./data/data.json"
    } else {
        filePath = '.' + req.url;
    }
    getResource(req, res, filePath);

}).listen(port, '0.0.0.0');
console.log('Server running at http://127.0.0.1:' + port + '/');

function saveTickets(req, res) {
    var body = [];
    req.on('data', function (chunk) {
        body.push(chunk);
    }).on('end', function () {
        var data = JSON.parse(body[0]);
        console.log(data)
        if (data.qnt >= 1 && data.type.length > 0) {
            res.writeHead(200);
        } else {
            res.writeHead(400);
        }
        res.end();
    });

}
function getResource(req, res, filePath) {

    if (filePath == './')
        filePath = './index.html';

    var extname = path.extname(filePath);
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
    }
    fs.readFile(filePath, function (error, content) {
        if (error) {
            res.writeHead(404);
            res.end('Sorry, check with the site admin for error: ' + error.code + ' ..\n');
            res.end();
        }
        else {
            res.writeHead(200, {'Content-Type': contentType});
            res.end(content, 'utf-8');
        }
    });
}