import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import webTheme from './styles/theme';

import store from './store';
import PurchaseTicket from "./containers/PurchaseTicket";

const theme = createMuiTheme(webTheme);

const App = () => (
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <PurchaseTicket />
        </MuiThemeProvider>
    </Provider>
);

ReactDOM.render(
    <App />,
    document.getElementById('root')
);