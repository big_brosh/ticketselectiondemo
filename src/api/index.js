import axios from 'axios';
import config from '../config';

const request = (method, url, configs) => (params) =>
    axios[method](`${config.baseURL}${url}`, params, configs);

export const tickets = {
    getTickets: request('get', '/data'),
    purchaseTickets: request('post', '/next')
};