import React from "react";
import { connect } from 'react-redux';

import {
    Select,
    MenuItem
} from "@material-ui/core";

import { setTicket } from "../../../actions/tickets";

const handleChange = (e, {tickets, setTicket}) => {
     tickets.forEach(({type}, index) => {
           if (type === e.target.value) {
               setTicket(index);
           }
     });
 };

const TicketSelect = (props) => {
    const { tickets, selected } = props;

    return (
        <Select
            value={tickets[selected].type}
            onChange={(e) => handleChange(e, props)}
        >
            {
                tickets.map(({ type }) => {
                    return (
                        <MenuItem
                            key={type}
                            value={type}
                        >
                            { type }
                        </MenuItem>
                    );
                })
            }
        </Select>
    )
};

const mapStateToProps = ({ tickets }) => {
    return {
        tickets: tickets.ticketsAvailable,
        selected: tickets.currentTicket
    }
};

export default connect(
    mapStateToProps,
    { setTicket }
)(TicketSelect);