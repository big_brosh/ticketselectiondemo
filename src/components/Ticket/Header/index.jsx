import React from 'react';
import { connect } from 'react-redux';

import { CardHeader } from '@material-ui/core';

const TicketHeader = ({ event }) => (
    <CardHeader title={event} />
);

const mapStateToProps = (state) => {
    return {
        event: state.tickets.event
    }
};

export default connect(mapStateToProps, null)(TicketHeader);