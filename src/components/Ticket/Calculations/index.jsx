import React from 'react';
import { connect } from 'react-redux';

import { CardHeader } from '@material-ui/core';

const TicketCalculations = ({
    ticketsAvailable,
    currentTicket,
    quantity
}) => {
    const ticket = ticketsAvailable[currentTicket];
    const { price } = ticket;
    const title = `Total: £${quantity * price} (${quantity} ${quantity === 1 ? 'ticket' : 'tickets'})`;

    return <CardHeader title={title} />
};

const mapStateToProps = (state) => {
    return {
        ticketsAvailable: state.tickets.ticketsAvailable,
        currentTicket: state.tickets.currentTicket,
        quantity: state.tickets.quantity
    }
};

export default connect(mapStateToProps, null)(TicketCalculations);