import React, { Component } from 'react';

import { Add, Remove } from '@material-ui/icons';
import { Button, Input } from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";

import styles from './styles';

class Counter extends Component {
	state = {
		count: this.props.value
	}

	increase = () => {
		this.setState(
			({count}) => ({ count: count + 1 }),
			() => { this.props.setValue(this.state.count) }
		)
	}

    decrease = () => {
        this.setState(
        	(prevState) => {
        		let count = prevState.count - 1;
        		count = count >= 1 ? count : 1;

				return { count };
			},
			() => { this.props.setValue(this.state.count); }
        )
    }

	handleInputChange = (e) => {
		let count = Number(e.target.value);
		count = count >= 1 ? count : 1;

		this.setState({ count }, () => { this.props.setValue(count) });
	}

	render() {
		const { count } = this.state;
		const { counter_wrap, input } = this.props.classes;

        return (
            <div className={counter_wrap}>
                <Button
                    variant="text"
                    size='small'
                    aria-label="decrease"
                    onClick={this.decrease}
                >
                    <Remove />
                </Button>

                <Input
                    value={count}
                    className={input}
                    onChange={this.handleInputChange}
                />

                <Button
                    variant="text"
                    size='small'
                    aria-label="increase"
                    onClick={this.increase}
                >
                    <Add />
                </Button>
            </div>
		);
	}
}

export default withStyles(styles)(Counter);