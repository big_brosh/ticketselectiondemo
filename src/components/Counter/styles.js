const styles = () => ({
    counter_wrap: {
        display: 'flex',
        alignItems: 'center'
    },
    input: {
        maxWidth: 45
    },
    button: {
        maxWidth: 20
    }
});

export default styles;