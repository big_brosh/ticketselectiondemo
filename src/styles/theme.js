import { blue, teal } from '@material-ui/core/colors';

export default {
    palette: {
      secondary: {
          main: teal[400]
      }
    },
    typography: {
        headline: {
            color: "#fff"
        }
    },
    overrides: {
        MuiCardHeader: {
            root: {
                color: "#fff",
                backgroundColor: blue[600],
                textAlign: 'center'
            }
        },
        MuiInput: {
            input: {
                paddingLeft: 6,
                paddingRight: 6
            }
        },
        MuiButton: {
            sizeSmall: {
                minWidth: 20
            },
            fab: {
                width: 40,
                height: 40,
            }
        }
    },
};