export const SET_TICKETS_QUANTITY = "SET_TICKETS_QUANTITY";
export const SET_TICKETS = "SET_TICKETS";
export const SET_TICKET = "SET_TICKET";
export const SET_EVENT = "SET_EVENT";

export function setTicketsQuantity(quantity) {
    return {
        type: SET_TICKETS_QUANTITY,
        quantity
    }
}

export function setTickets(tickets) {
    return {
        type: SET_TICKETS,
        tickets
    }
}

export function setTicket(ticket) {
    return {
        type: SET_TICKET,
        ticket
    }
}

export function setEvent(event) {
    return {
        type: SET_EVENT,
        event
    }
}
