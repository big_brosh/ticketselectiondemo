import { createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import ticketsReducer from "../reducers/tickets";

const store = createStore(
    combineReducers({
        tickets: ticketsReducer
    }),
    composeWithDevTools()
);

export default store;