const styles = () => ({
    card: {
        maxWidth: 320,
        borderRadius: 0
    },
    content: {
        minHeight: 330,
        display: 'flex',
        flexDirection: 'column'
    },
    subHeader: {
        fontWeight: 'initial'
    },
    selectWrap: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    buttonWrap: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flexGrow: 1
    }
});

export default styles;