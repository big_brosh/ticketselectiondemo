import React, { Component, Fragment } from 'react';

import { Button, Card, CardContent } from '@material-ui/core';
import { ArrowForward } from "@material-ui/icons";
import { withStyles } from '@material-ui/core/styles';

import TicketHeader from "../../components/Ticket/Header";
import TicketCalculations from "../../components/Ticket/Calculations";
import TicketSelect from "../../components/Ticket/Select";
import {tickets} from "../../api";
import connect from "react-redux/es/connect/connect";
import {
	setEvent,
	setTicket,
	setTickets,
	setTicketsQuantity
} from "../../actions/tickets";
import Counter from "../../components/Counter";

import styles from './styles';

class PurchaseTicket extends Component {
   state = {
      isSubmit: false
   }

   componentDidMount() {
      this.fetchTicketsData();
   }

   fetchTicketsData = async() => {
      const { setTickets, setEvent, setTicketsQuantity } = this.props;
      const response = await tickets.getTickets();
      const { tickets: availableTickets, event } = response.data;

      setTickets(availableTickets);
      setEvent(event);
   }

   handleCounterChange = (quantity) => {
       this.props.setTicketsQuantity(quantity);
   };

    submitPurchase = async() => {
        const { selected, quantity } = this.props;

        const data = {
            qnt: quantity,
            type: this.props.tickets[selected].type
        };

        await tickets.purchaseTickets(data);

        this.setState({
            isSubmit: true
        });
    }

   render() {
        const { isSubmit } = this.state;
        const {
            classes,
            quantity,
            tickets,
            selected
        } = this.props;
        const {
            card,
            content,
            subHeader,
            selectWrap,
            buttonWrap
        } = classes;

        const type = tickets[selected].type;

        return (
            <Card className={card}>
                <TicketHeader />

                <CardContent className={content}>
                    {
                        !isSubmit ? (
                            <Fragment>
                                <h3 className={subHeader}>Choose your tickets</h3>

                                <div className={selectWrap}>
                                    <TicketSelect />
                                    <Counter
                                        value={quantity}
                                        setValue={this.handleCounterChange}
                                    />
                                </div>

                                <div className={buttonWrap}>
                                    <Button
                                        variant="fab"
                                        size='small'
                                        aria-label="decrease"
                                        color="secondary"
                                        onClick={this.submitPurchase}
                                    >
                                        <ArrowForward />
                                    </Button>
                                </div>
                            </Fragment>
                        ) : (
                            <p>Great! You have bought {quantity} {type} { quantity === 1 ? 'ticket' : 'tickets' }.</p>
                        )
                    }
                </CardContent>

                <TicketCalculations />
            </Card>
        )
    }
}

const mapStateToProps = ({ tickets }) => {
    return {
        tickets: tickets.ticketsAvailable,
        selected: tickets.currentTicket,
        quantity: tickets.quantity
    }
};

export default connect(
   mapStateToProps,
   {
        setTickets,
        setTicket,
        setEvent,
        setTicketsQuantity
   }
)(withStyles(styles)(PurchaseTicket));