import {
	SET_TICKETS_QUANTITY,
	SET_TICKET,
	SET_TICKETS,
	SET_EVENT
} from "../actions/tickets";

const initialState = {
    event: 'Funny Event',
    ticketsAvailable: [{ type: '', price: '0' }],
    currentTicket: 0,
    quantity: 1
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_TICKETS_QUANTITY: {
            let { quantity } = action;
            quantity = quantity >= 1 ? quantity : 1;

            return { ...state, quantity };
        }

        case SET_TICKET:
            const { ticket } = action;
            return { ...state, currentTicket: ticket };

        case SET_TICKETS:
            const { tickets } = action;
            return { ...state, ticketsAvailable: tickets };

        case SET_EVENT:
            const { event } = action;
            return { ...state, event };

        default:
            return state;
    }
}